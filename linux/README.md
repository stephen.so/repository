# Linux-related

## Index

- [Finding the size of a directory or file](#finding-the-size-of-a-directory-or-file)
- [Creating and extracting a tar file](#creating-and-extracting-a-tar-file)
- [Compressing files using gzip](#compressing-files-using-gzip)
- [Bash shell and scripting](#bash-shell-and-scripting)
- [Setting up FTP with TLS (FTPS)](#setting-up-ftp-with-tls-(ftps))
- [Adding users and groups](#adding-users-and-groups)
- [NIS setup for centralised user authentication](#setting-up-nis-for-centralised-user-authentication)
- [Setting up new VPN in linux (Source: https://github.com/hwdsl2/setup-ipsec-vpn/blob/master/docs/clients-xauth.md#linux)](clients-xauth.md)
- [Starting the VNC server](#starting-the-vnc-server)
- [Listing and killing VNC servers](#listing-and-killing-vnc-servers)
- [Connecting to VNC server from a client](#connecting-to-vnc-server-from-a-client)

---

## Finding the size of a directory or file

In order to determine the size of a file, you can use the ls command.  Change directory (cd) into the directory of where the file is stored and type:

```
ls -lh
```

In order to determine the total size of a directory (and all of its files and subdirectories), go to the directory above the one that you are interested in and type:

```
du -h directory_name
```
---

## Creating and extracting a tar file

A common way of creating an archive of an entire directory is to convert it to a *tar* file, also known as "tarring".  This tar file (also known as a **tarball**) preserves all filenames, creation and modification dates, etc.

### Creating a tar file

Let's say we want to 'tar up' a directory called ```project``` and store it as ```project.tar``` (make sure you in the directory above ```project``` when running the following command).

```
tar cvf project.tar project
```

### Extracting a tar file

In order to extra all the contents of the tar files in the current directory.

```
tar xvf project.tar
```

---


## Compressing files using gzip

Compressing files (especially tar files) is especially useful when we wish to transfer them to another computer.  The original compression utility is called ```gzip```.  Note that ```gzip``` only compresses **one file**, so you will need to convert multiple files into one file using tar first.

To compress the ```project.tar``` file using gzip (using best compression)

```
gzip --best project.tar
```
(this creates a ```project.tar.gz``` file)

To decompress:

```
gunzip project.tar.gz
```

You can also decompress and untar using the tar utility as well

```
tar zxvf project.tar.gz
```

There is a multithreaded version of gzip called ```pigz```.  Just substitute ```gzip``` with ```pigz``` in the commands above.


---

## Bash shell and scripting

- [Fedora magazine on Bash shell configuration](https://fedoramagazine.org/customizing-bash/)

---


## Setting up FTP with TLS (FTPS)

(This content comes from [this page](https://www.howtoforge.com/tutorial/pureftpd-tls-on-centos/))

Install the pure-ftp server and OpenSSL from dnf

```
dnf install pure-ftpd ssl
```

Configure pureftpd by editing /etc/pure-ftpd/pure-ftpd.conf and set TLS to 2

```
# This option can accept three values :
# 0 : disable SSL/TLS encryption layer (default).
# 1 : accept both traditional and encrypted sessions.
# 2 : refuse connections that don't use SSL/TLS security mechanisms,
#     including anonymous sessions.
# Do _not_ uncomment this blindly. Be sure that :
# 1) Your server has been compiled with SSL/TLS support (--with-tls),
# 2) A valid certificate is in place,
# 3) Only compatible clients will log in.

TLS                      2
```

Remove the # from the following two lines

```
TLSCipherSuite           HIGH
CertFile                 /etc/ssl/private/pure-ftpd.pem
```

Then remove the # in front of the following 2 lines:

```
TLSCipherSuite           HIGH
CertFile                 /etc/ssl/private/pure-ftpd.pem
```

and save the altered configuration file.



In order to use TLS, we must create an SSL certificate. I create it in ```/etc/ssl/private/```, therefore I create that directory first:

```
mkdir -p /etc/ssl/private/
```

Afterwards, we can generate the SSL certificate as follows:

```
openssl req -x509 -nodes -days 7300 -newkey rsa:4096 -keyout /etc/ssl/private/pure-ftpd.pem -out /etc/ssl/private/pure-ftpd.pem
```


Change the permissions of the SSL certificate:

```
chmod 600 /etc/ssl/private/pure-ftpd.pem
```

Finally, restart PureFTPd:

```
systemctl restart pure-ftpd.service
```

---
## Adding users and groups

- To create a new user and assign a group, while changing their password (as root)
```
useradd -G group user
passwd user
```
- To create a user with custom comments (such as full name, phone number, etc.)
```
useradd -c "John Smith" john
```
- To create a new user with different home directory
```
useradd -d /data/user user
```
- To create user with a specific user ID (UID)
```
useradd -u 1234 user
```
- To add an existing user account to a group (secondary) or multiple groups
```
usermod -a -G group user
usermod -a -G group1,group2,group3 user
```
- To change the **primary** group for an existing user
```
usermod -g group user
```
- To disable a user account
```
usermod -s /sbin/nologin user
```
---

## Setting up NIS for centralised user authentication

NIS (Network Information Service), also known as "yellow pages" and developed by Sun Microsystems, it allows a central server (NIS server) and some slave servers to centrally store user account information (username, passwords, etc.).  Client machines can then authenticate from this NIS server.

Duplicated from [server-world.info](https://www.server-world.info/en/note?os=CentOS_8&p=nis&f=1)

### Setting up the NIS server

- Install the required software (ypserv, rpcbind) and configure the NIS server
```
[root@dlp ~]# dnf -y install ypserv rpcbind

# set NIS domain
[root@dlp ~]# ypdomainname srv.world

[root@dlp ~]# echo "NISDOMAIN=srv.world" >> /etc/sysconfig/network

[root@dlp ~]# vi /var/yp/securenets

# create new
# specify range of network you allow to access NIS clients

255.0.0.0       127.0.0.0
255.255.255.0   10.0.0.0

[root@dlp ~]# vi /etc/hosts
# add hosts that are in NIS domain (server/client)

#
10.0.0.30   dlp.srv.world dlp
10.0.0.51   node01.srv.world node01
.....

[root@dlp ~]# systemctl enable --now rpcbind ypserv ypxfrd yppasswdd nis-domainname
# update NIS databases

[root@dlp ~]# /usr/lib64/yp/ypinit -m

At this point, we have to construct a list of the hosts which will run NIS
servers.  dlp.srv.world is in the list of NIS server hosts.  Please continue to add
the names for the other hosts, one per line.  When you are done with the
list, type a <control D>.
        next host to add:  dlp.srv.world
        next host to add:  # Ctrl + D key
The current list of NIS servers looks like this:

dlp.srv.world

Is this correct?  [y/n: y]  y
We need a few minutes to build the databases...
Building /var/yp/srv.world/ypservers...
Running /var/yp/Makefile...
gmake[1]: Entering directory '/var/yp/srv.world'
Updating passwd.byname...
Updating passwd.byuid...
Updating group.byname...
Updating group.bygid...
Updating hosts.byname...
Updating hosts.byaddr...
Updating rpc.byname...
Updating rpc.bynumber...
Updating services.byname...
Updating services.byservicename...
Updating netid.byname...
Updating protocols.bynumber...
Updating protocols.byname...
Updating mail.aliases...
gmake[1]: Leaving directory '/var/yp/srv.world'

dlp.srv.world has been set up as a NIS master server.

Now you can run ypinit -s dlp.srv.world on all slave server.

```
- Whenever adding a local user/group or new hosts in ```/etc/hosts``` on the NIS server, then you will need to apply changes to the NIS databases
```
[root@dlp ~]# cd /var/yp
[root@dlp yp]# make 
```
- If SELinux is enabled
```
[root@dlp ~]# setsebool -P nis_enabled on
[root@dlp ~]# setsebool -P domain_can_mmap_files on 
```
- If firewalld is running, it needs to allow NIS services or ports (which need to be fixed)
```
[root@dlp ~]# vi /etc/sysconfig/network
 
# add to the end
YPSERV_ARGS="-p 944"
YPXFRD_ARGS="-p 945"
[root@dlp ~]# vi /etc/sysconfig/yppasswdd

# add like follows
YPPASSWDD_ARGS="--port 950"

[root@dlp ~]# systemctl restart rpcbind ypserv ypxfrd yppasswdd
[root@dlp ~]# firewall-cmd --add-service=rpc-bind --permanent
[root@dlp ~]# firewall-cmd --add-port={944-951/tcp,944-951/udp} --permanent
[root@dlp ~]# firewall-cmd --reload 
```

### Setting up the NIS client

- Install ypbind and configure the NIS client (also need to add server to ```/etc/hosts```)
```
 [root@node01 ~]# dnf -y install ypbind rpcbind oddjob-mkhomedir

# set NIS domain
[root@node01 ~]# ypdomainname srv.world
[root@node01 ~]# echo "NISDOMAIN=srv.world" >> /etc/sysconfig/network
[root@node01 ~]# vi /etc/yp.conf

# add to the end
# [domain (NIS domain) server (NIS server)]
domain srv.world server dlp.srv.world

[root@node01 ~]# authselect select nis --force
Backup stored at /var/lib/authselect/backups/2019-10-17-01-18-26.zBmEkS
Profile "nis" was selected.
The following nsswitch maps are overwritten by the profile:
- aliases
- automount
- ethers
- group
- hosts
- initgroups
- netgroup
- networks
- passwd
- protocols
- publickey
- rpc
- services
- shadow

Make sure that NIS service is configured and enabled. See NIS documentation for more information.

# set if you need (create home directory when initial login)
[root@node01 ~]# authselect enable-feature with-mkhomedir

# if SELinux is enabled, change boolean setting
[root@node01 ~]# setsebool -P nis_enabled on
[root@node01 ~]# systemctl enable --now rpcbind ypbind nis-domainname oddjobd
[root@node01 ~]# exit

CentOS Linux 8 (Core)
Kernel 4.18.0-80.7.1.el8_0.x86_64 on an x86_64

node01 login: redhat     # NIS user
Password:
[redhat@node01 ~]$       # just logined

# confirm binded NIS server
[redhat@node01 ~]$ ypwhich

dlp.srv.world

# change NIS password
[redhat@node01 ~]$ yppasswd
Changing NIS account information for redhat on dlp.srv.world.
Please enter old password:     # current password
Changing NIS password for redhat on dlp.srv.world.
Please enter new password:     # new one
Please retype new password:

The NIS password has been changed on dlp.srv.world.

```

## Starting the VNC server

These instructions will allow you to set up a VNC server, which will allow you to remotely connect and use graphical applications.

In Fedora, you can install using: ```dnf install tigervnc-server```

### Step 1: Setting up a password

It is important to set the VNC password to protect your session.  You can do so by running the command:

```
vncpasswd
```

Note that VNC passwords are limited to **8 characters in length**.  This will create a hidden directory in your home called ```.vnc```, which contains your password as well as any configuration files.

### Step 2:  Starting the server

Next, you need to start the VNC server.  The server requires a **display number** (e.g.  :1, :5, :8), which determines which TCP port you will need to use.

All VNC connections use port 5900+x, where x is your display number.  For example, if you use a display number of :5, then you need to connect to port 5905.

To start the server, with a display size of 1440x900:

```
vncserver -fg -geometry 1440x900
```

This will start the server.  You will need to take note of the display number of this server, as you can see in the following example output:

```
$ vncserver -fg -geometry 1440x900

New 'graviton:1 (stephen)' desktop is graviton:1

Starting applications specified in /home/stephen/.vnc/xstartup
Log file is /home/stephen/.vnc/graviton:1.log

```

We can see the ":1" next the hostname, which indicates **display :1**.  The server will continue running in the foreground until you kill it by pressing control-c.



## Connecting to VNC server from a client

You will need the VNC client to connect to a remote VNC server.

In Fedora, you can install using: ```dnf install tigervnc```.
In Windows, you can download a free vncviewer program, [RealVNC VNC viewer](https://www.realvnc.com/en/connect/download/viewer/windows/)

For security reasons, you cannot **directly connect to the server** since VNC traffic is **not encrypted**.  Therefore, you will need to set up an SSH tunnel.

### Setting up an SSH tunnel under Linux

An SSH tunnel connects a TCP port on your local machine to a TCP port on the remote machine using an encrypted connection.  

For example, let us say we wish to connect to port 5901 on the remote server (graviton).  We can connect that port to a local port using an SSH tunnel.  It is best to choose a local port number greater than 10000 (say, 12345).  So we can set up the SSH tunnel that connects local port 12345 to remote port 5901:

```
ssh -L 12345:localhost:5901 132.234.165.13
```

Note that ```localhost``` is a shortcut for your local machine

### Setting up an SSH tunnel under Windows

Under Windows, you can set up an SSH tunnel using Putty.  You will need to set the SSH options as shown below (remember to click the "Add" button)

![Setting up SSH tunnelling in putty](putty.png)



### Connecting to the remote VNC server over SSH tunnel under Linux

After we have created the SSH tunnel, we connect the VNC viewer to ```localhost:port```.  For example, assuming we have a VNC server on display number :1, we first start the SSH tunnel:

```
ssh -L 12345:localhost:5901 132.234.165.13
```

Then we enter the localhost and local port into the vncviewer program.

![vncviewer program on linux](vncviewer.png)

Enter the password for the vnc server.

![Entering VNC password](vncpasswd.png)

This should open up the remote desktop.

![VNC desktop](vncconnect.png)

### Connecting to the remote VNC server over SSH tunnel under Windows

After setting up the SSH tunnel in putty, log into the remote server using putty.

Then enter the following address in VNC viewer.

![VNC viewer under Windows](winvnc.png)


## Listing and killing VNC servers

### Killing foreground servers

By using the ```-fg``` when starting the VNC server, the server is in the foreground so you can kill it by pressing Ctrl-C.


### Killing background servers

However, if you forgot to use ```-fg```, then the server runs in the background and you will need to kill it manually, as shown below.

In order to see how many VNC servers you have started and what their display numbers, use ```vncserver -list```

Here is some example output:

```
$ vncserver -list

TigerVNC server sessions:

X DISPLAY #	PROCESS ID
:1		134366
```

This shows us that there is one server with display number :1

It is **recommended that you kill the server** if you have finished using it, to free up some ports.  In order to stop the VNC server, you will need to know the display number

```
vncserver -kill :1
```
