# The Repository

This repository stores useful code snippets, handy setup guides, and useful knowledge in the areas of linux, HPC (high performance computing), python, etc.  In other words, it acts as a central storage for all things useful that I've encountered.

## Index

- [Linux-related](linux/README.md)
- [HPC (high performance computing)](HPC/README.md)
- Tensorflow
- [LaTeX](latex/README.md)
