# LaTeX information

This repository stores useful code snippets and tips for doing stuff in LaTeX typesetting.

## Index

- [Tables](#tables)

---


## Tables

- [Fedora Magazine article on LaTeX tables](https://fedoramagazine.org/latex-typesetting-part-2-tables/)
