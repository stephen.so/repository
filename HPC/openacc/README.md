# OpenACC

OpenACC is parallelisation library that can be called from C and Fortran.  Like OpenMP, it uses ```pragma``` directives to instruct the compile to parallelise for loops as well as specify which data is to be copied.  In contrast with OpenMP, which is mainly focused on CPU-based parallelisation, OpenACC can generate GPU code as well, without having to write low-level code in CUDA or OpenCL.


## OpenACC resources

- ["Introduction to OpenACC" by John Urbanic from Pittsburg Supercomputing Centre](OpenACC_Introduction_To_OpenACC.pdf)
- ["OpenACC online course" by Robert Searles from NVIDIA](OpenACC-Course-2020-Module-1.pdf)
- - ["OpenACC online course" presentation video](OpenACC-Course-2020-Module-1.mp4)
