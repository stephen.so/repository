# ```ipyparallel``` repository

```ipyparallel``` allows you to distribute python-based computations across a number of difference machines (cluster computing) in order to perform parallel-based computing.

## Index

- [Architecture](#architecture)
- [Setup](#setup)
- [Starting up the cluster](#starting-up-the-cluster)

---


## Architecture

The architecture of ```ipyparallel``` is shown in the figure below

![Architecture of ipyparallel](ipyparallel.png)
- **Engine**: These are python3 instances that are started on different machines that do the computations
- **Hub**:  The hub is the central location that manages your cluster, i.e. passes data from clients to the engine and vice versa
- **Client**:  These are your Python programs that wish to access the ```ipyparallel```
- **Schedulers**: These perform scheduling on your programs to load balance them across the different engines

## Setup

### Installation

If you are in Fedora and wish to install it into your system-wide python

```
dnf install python3-ipyparallel
```

If you would like to install in a virtualenv (say in ```py38```)

```
source py38/bin/activate
pip3 install ipyparallel
```

### Setting up profiles and configuration

We first need to make an IPython cluster profile.  We'll call it ```ssh``` here.

```
ipython profile create --parallel --profile=ssh
```

This creates a directory in your home called ```~/.ipython/profile_ssh```

In this profile directory, there will be a number of files:

```
-rw-rw-r-- 1 stephen stephen 23445 Jun 21 23:49 ipcluster_config.py
-rw-rw-r-- 1 stephen stephen 15681 Jun 21 23:44 ipcontroller_config.py
-rw-rw-r-- 1 stephen stephen 18537 Jun 21 22:44 ipengine_config.py
-rw-rw-r-- 1 stephen stephen 24275 Jun 21 22:44 ipython_config.py
-rw-rw-r-- 1 stephen stephen 18815 Jun 21 22:44 ipython_kernel_config.py
drwxrwxr-x 2 stephen stephen  4096 Jun 21 23:04 log
drwx------ 2 stephen stephen  4096 Jun 21 23:49 pid
drwx------ 2 stephen stephen  4096 Jun 21 23:49 security
drwxrwxr-x 2 stephen stephen  4096 Jun 21 22:44 startup
```
There are two ways to start the ipyparallel cluster:
- (Simple way) using ```icluster```
- (Manual way) using ```ipcontroller``` and ```ipengine```

The configuration for the first way involves modifying parts of **two files**: ```ipcluster_config.py``` and ```ipcontroller_config```

In the ```ipcluster_config.py``` file, add the following lines (by uncommenting). Note that we are assuming the **hub** and **controller** are running on the same machine (192.168.1.7), and we are connecting to each engine using an _ssh tunnel_.

```
c.IPClusterEngines.engine_launcher_class = 'SSH'
c.IPClusterStart.controller_ip = '192.168.1.7'
c.SSHEngineSetLauncher.engines = {"192.168.1.64" : {'n': 4, 'engine_cmd' : ['/home/stephen/python/py36/bin/python3', '-m', 'ipyparallel.engine']}}
```

The last option is a dictionary where we specify which computers to use and how many engines to start (usually the number of processing cores available).  Here is an example of three machines with 8 engines.

```
c.SSHEngineSetLauncher.engines = {192.168.1.2 : 8,
			192.168.1.3 : 8,
			192.168.1.4 : 8}
```

If we are running Python on virtualenv on each machine, we need to specify another dictionary inside and setting the ```'engine_cmd'``` to point to the python executable within the virtualenv (named ```py36``` in this example).

In the ```ipcontroller.config``` file, we need to specify where the controller will reside, so the engines know where it is

```
c.IPControllerApp.engine_ssh_server = '192.168.1.7'
```

### Shared filesystem

The previous set up will copy required files over SSH.  If all the machines share the same filesystem (NFS), then you can skip the movement of files by specifying empty lists:

```
c.SSHLauncher.to_send = []
c.SSHLauncher.to_fetch = []
```

### Setting up passwordless ssh access

Since all network communication is perform over ssh tunnels, you should set up _password-less ssh_ across all machines using ```ssh-keygen``` and saving them in the ```authorized_keys``` files.  This will avoid having to enter the ssh password multiple times when starting up the cluster.


## Starting up the cluster

On the **hub** computer (192.168.1.7 in this example), you can start the cluster by typing:

```
ipcluster start --profile=ssh
```
