# Singularity container commands

Singularity containers are useful for transporting an entire and exact programming environment to different machines where you may have the software installed or do not have control over installation (i.e. the Griffith HPC)

For more information, have a look at the [Sylab.io user guides](https://sylabs.io/guides/3.5/user-guide/quick_start.html#quick-installation-steps)

- Making a singularity container (as directory):
```
singularity build --sandbox container_name container.def
```
- Making a singularity container (as sif image file): 
```
singularity build container.sif container.def
```
- Convert sandbox directory into image files:
 ```
 singularity build container.sif container_name
 ```
- Running singularity container: 
```
singularity shell container.sif
```
- Run container (sandbox) as writable to make changes
```
singularity shell --writable container_name
```

- Running singularity container (with nvidia link): 
```
singularity shell --nv container.sif
```
