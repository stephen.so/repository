# High performance computing (HPC) repository

This is a repository for anything related to HPC


## Index

- [ipyparallel](ipyparallel.md)
- [Singularity](singularity.md)
- [OpenACC](openacc/README.md)
- [OpenMP](openmp/README.md)
